import * as child_process from "child_process";
import {
  DockerComposeEnvironment,

  StartedDockerComposeEnvironment, Wait
} from "testcontainers";
import { promisify } from "util";

const exec = promisify(child_process.exec);

describe("ExampleTest", () => {
  const CONTAINER_NAME = "issue_reproduction_postgres";
  let environment: StartedDockerComposeEnvironment;
  beforeAll(async (done) => {
    jest.setTimeout(30_000);
    console.log(`Starting dependencies`);
    const composeFile = "docker-compose.yml";
    console.log(`__dirname: ${__dirname}`);
    environment = await new DockerComposeEnvironment(__dirname, composeFile)
      .withWaitStrategy(
        CONTAINER_NAME,
        Wait.forLogMessage("database system is ready to accept connections")
      )
      .up();

    const psOutput = await exec("docker ps");
    console.log(`docker ps:\n ${psOutput.stdout}`);
    const inspectOutput = await exec(`docker inspect ${CONTAINER_NAME}`);
    console.log(`docker inspect ${CONTAINER_NAME}:\n ${inspectOutput.stdout}`);

    const container = environment.getContainer(CONTAINER_NAME);
    process.env.DB_HOST = container.getContainerIpAddress();
    process.env.DB_PORT = String(container.getMappedPort(5432));
    console.log(`Done starting dependencies`);
    done();
  });

  it("env vars are set", () => {
    console.log(`Running tests`);
    expect(process.env.DB_HOST).toBeDefined();
    expect(process.env.DB_HOST).not.toBeNull();
    expect(process.env.DB_PORT).toBeDefined();
    expect(process.env.DB_PORT).not.toBeNull();
  });

  afterAll(async (done) => {
    console.log(`Shutting down dependencies in docker`);
    await environment.down();
    done();
  });
});
